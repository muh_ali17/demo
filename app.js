const http = require('http'),
    url = require('url'),
    conn = require('./config/conn.js'),
    fs = require('fs');

const server = http.createServer();

server.on('request', function (req, res) {
    const method = req.method;

    const current_url = url.parse(req.url, true);
    const pathname = current_url.pathname;
    const search_params = current_url.query;

    if(method === 'POST' && pathname === '/products') {
        sql = "INSERT INTO products (name, price, stock, discount) VALUES ?";
        let values = [
            [search_params.name, search_params.price, search_params.stock, search_params.discount]
        ];

        conn.query(sql, [values],function (err, result) {
            if (err) throw err;

            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.end();
        });
    }
});

server.listen(8080);